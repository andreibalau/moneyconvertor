package moneyconvertor;

import java.util.HashMap;
import java.util.Map;

public enum Currency implements ICurencyConverter {

	EUR {
		public Map<String, Double> convert(Double insertedVal) {
			eur = insertedVal;
			usd = 1.12;
			ron = 4.73;
			chf = 1.09;
			gbp = 0.92;
			mapOfCurrencyValues = new HashMap<String, Double>();
			mapOfCurrencyValues.put("eur", insertedVal);
			mapOfCurrencyValues.put("usd", insertedVal * usd);
			mapOfCurrencyValues.put("ron", insertedVal * ron);
			mapOfCurrencyValues.put("chf", insertedVal * chf);
			mapOfCurrencyValues.put("gbp", insertedVal * gbp);
			return mapOfCurrencyValues;
		}
	},
	USD {
		public Map<String, Double> convert(Double insertedVal) {
			usd = insertedVal;
			eur = 0.89;
			ron = 4.21;
			chf = 0.97;
			gbp = 0.82;
			mapOfCurrencyValues = new HashMap<String, Double>();
			mapOfCurrencyValues.put("usd", insertedVal);
			mapOfCurrencyValues.put("eur", insertedVal * eur);
			mapOfCurrencyValues.put("ron", insertedVal * ron);
			mapOfCurrencyValues.put("chf", insertedVal * chf);
			mapOfCurrencyValues.put("gbp", insertedVal * gbp);
			return mapOfCurrencyValues;
		}
	},
	RON {
		public Map<String, Double> convert(Double insertedVal) {
			ron = insertedVal;
			eur = 0.21;
			usd = 0.24;
			chf = 0.23;
			gbp = 0.20;
			mapOfCurrencyValues = new HashMap<String, Double>();
			mapOfCurrencyValues.put("ron", insertedVal);
			mapOfCurrencyValues.put("eur", insertedVal * eur);
			mapOfCurrencyValues.put("usd", insertedVal * usd);
			mapOfCurrencyValues.put("chf", insertedVal * chf);
			mapOfCurrencyValues.put("gbp", insertedVal * gbp);
			return mapOfCurrencyValues;
		}
	},
	CHF {
		public Map<String, Double> convert(Double insertedVal) {
			chf = insertedVal;
			eur = 0.92;
			usd = 1.03;
			ron = 4.34;
			gbp = 0.85;
			mapOfCurrencyValues = new HashMap<String, Double>();
			mapOfCurrencyValues.put("chf", insertedVal);
			mapOfCurrencyValues.put("eur", insertedVal * eur);
			mapOfCurrencyValues.put("usd", insertedVal * usd);
			mapOfCurrencyValues.put("ron", insertedVal * ron);
			mapOfCurrencyValues.put("gbp", insertedVal * gbp);
			return mapOfCurrencyValues;
		}
	},
	GBP {
		public Map<String, Double> convert(Double insertedVal) {
			gbp = insertedVal;
			eur = 1.08;
			usd = 1.22;
			ron = 5.12;
			chf = 1.18;
			mapOfCurrencyValues = new HashMap<String, Double>();
			mapOfCurrencyValues.put("gbp", insertedVal);
			mapOfCurrencyValues.put("eur", insertedVal * eur);
			mapOfCurrencyValues.put("usd", insertedVal * usd);
			mapOfCurrencyValues.put("ron", insertedVal * ron);
			mapOfCurrencyValues.put("chf", insertedVal * chf);
			return mapOfCurrencyValues;
		}
	};

	public double insertedVal;
	Map<String, Double> mapOfCurrencyValues;
	Double usd;
	Double eur;
	Double ron;
	Double chf;
	Double gbp;

}
