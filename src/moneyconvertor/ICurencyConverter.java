package moneyconvertor;

import java.util.Map;

public interface ICurencyConverter {
	public Map<String, Double> convert(Double insertedVal);
}
