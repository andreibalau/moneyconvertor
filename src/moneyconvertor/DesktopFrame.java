package moneyconvertor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

public class DesktopFrame {

	private JFrame frame;
	private JPanel mainFramePanel;
	private SpringLayout layout;
	private JTextField insertedVal;
	private JComboBox<Currency> CUR;
	private JButton convertButton;
	private JLabel eur;
	private JLabel usd;
	private JLabel ron;
	private JLabel chf;
	private JLabel gbp;

	public Double getInsertedVal() {
		Double insertedValParsed = Double.parseDouble(insertedVal.getText());
		return insertedValParsed;
	}

	public DesktopFrame() {
		frame = new JFrame("Money Convertor");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		mainFramePanel = new JPanel();
		layout = new SpringLayout();

		mainFramePanel.setBorder(BorderFactory.createLineBorder(Color.black));
		mainFramePanel.setLayout(layout);

		insertedVal = new JTextField("Value");// TODO try - catch the string inputs
		insertedVal.setPreferredSize(new Dimension(80, 40));
		insertedVal.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				insertedVal.setText("");
			}
		});
		mainFramePanel.add(insertedVal);

		convertButton = new JButton("Convert");
		convertButton.setPreferredSize(new Dimension(80, 40));
		convertButton.addActionListener(e -> convertAction());
		mainFramePanel.add(convertButton);

		Currency[] currencyList = { Currency.EUR, Currency.RON, Currency.USD, Currency.CHF, Currency.GBP };
		CUR = new JComboBox<Currency>(currencyList);
		CUR.setPreferredSize(new Dimension(80, 40));
		CUR.setSelectedIndex(4);
		mainFramePanel.add(CUR);

		/*--layout for JTextField insertderVal, JComboBox CUR, JButton convert*/
		layout.putConstraint(SpringLayout.VERTICAL_CENTER, insertedVal, -200, SpringLayout.VERTICAL_CENTER,
				mainFramePanel);
		layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, insertedVal, 0, SpringLayout.HORIZONTAL_CENTER,
				mainFramePanel);
		layout.putConstraint(SpringLayout.WEST, CUR, 30, SpringLayout.EAST, insertedVal);
		layout.putConstraint(SpringLayout.VERTICAL_CENTER, CUR, -200, SpringLayout.VERTICAL_CENTER, mainFramePanel);
		layout.putConstraint(SpringLayout.NORTH, convertButton, 20, SpringLayout.SOUTH, insertedVal);
		layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, convertButton, 0, SpringLayout.HORIZONTAL_CENTER,
				mainFramePanel);
		/*--------------------*/

		eur = new JLabel();
		usd = new JLabel();
		ron = new JLabel();
		chf = new JLabel();
		gbp = new JLabel();

		JLabel[] currencyLi = { eur, usd, ron, chf, gbp };

		for (JLabel l : currencyLi) {
			l.setFont(new Font("Serif", Font.BOLD, 14));
			mainFramePanel.add(l);
		}

		/*--layout for JLabels (eur,usd,ron,chf,gbp)*/
		layout.putConstraint(SpringLayout.NORTH, eur, 50, SpringLayout.SOUTH, convertButton);
		layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, eur, 0, SpringLayout.HORIZONTAL_CENTER, mainFramePanel);
		layout.putConstraint(SpringLayout.NORTH, usd, 10, SpringLayout.SOUTH, eur);
		layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, usd, 0, SpringLayout.HORIZONTAL_CENTER, mainFramePanel);
		layout.putConstraint(SpringLayout.NORTH, ron, 10, SpringLayout.SOUTH, usd);
		layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, ron, 0, SpringLayout.HORIZONTAL_CENTER, mainFramePanel);
		layout.putConstraint(SpringLayout.NORTH, chf, 10, SpringLayout.SOUTH, ron);
		layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, chf, 0, SpringLayout.HORIZONTAL_CENTER, mainFramePanel);
		layout.putConstraint(SpringLayout.NORTH, gbp, 10, SpringLayout.SOUTH, chf);
		layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, gbp, 0, SpringLayout.HORIZONTAL_CENTER, mainFramePanel);
		/*--------------------*/

		mainFramePanel.setLayout(layout);

		frame.add(mainFramePanel, BorderLayout.CENTER);
		frame.setSize(600, 700);
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);

	}

	private void convertAction() {

		String currencyToConvert = CUR.getSelectedItem().toString();

		switch (currencyToConvert) {
		case "EUR":
			HashMap<String, Double> eurToAllCurrencies = (HashMap<String, Double>) Currency.EUR
					.convert(getInsertedVal());

			eur.setText(String.format("EUR = %s", eurToAllCurrencies.get("eur")));
			usd.setText(String.format("USD = %s", eurToAllCurrencies.get("usd")));
			ron.setText(String.format("RON = %s", eurToAllCurrencies.get("ron")));
			chf.setText(String.format("CHF = %s", eurToAllCurrencies.get("chf")));
			gbp.setText(String.format("GBP = %s", eurToAllCurrencies.get("gbp")));
			break;
		case "USD":
			HashMap<String, Double> usdToAllCurrencies = (HashMap<String, Double>) Currency.USD
					.convert(getInsertedVal());

			eur.setText(String.format("EUR = %s", usdToAllCurrencies.get("eur")));
			usd.setText(String.format("USD = %s", usdToAllCurrencies.get("usd")));
			ron.setText(String.format("RON = %s", usdToAllCurrencies.get("ron")));
			chf.setText(String.format("CHF = %s", usdToAllCurrencies.get("chf")));
			gbp.setText(String.format("GBP = %s", usdToAllCurrencies.get("gbp")));
			break;
		case "RON":
			HashMap<String, Double> ronToAllCurrencies = (HashMap<String, Double>) Currency.RON
					.convert(getInsertedVal());

			eur.setText(String.format("EUR = %s", ronToAllCurrencies.get("eur")));
			usd.setText(String.format("USD = %s", ronToAllCurrencies.get("usd")));
			ron.setText(String.format("RON = %s", ronToAllCurrencies.get("ron")));
			chf.setText(String.format("CHF = %s", ronToAllCurrencies.get("chf")));
			gbp.setText(String.format("GBP = %s", ronToAllCurrencies.get("gbp")));
			break;
		case "CHF":
			HashMap<String, Double> chfToAllCurrencies = (HashMap<String, Double>) Currency.CHF
					.convert(getInsertedVal());

			eur.setText(String.format("EUR = %s", chfToAllCurrencies.get("eur")));
			usd.setText(String.format("USD = %s", chfToAllCurrencies.get("usd")));
			ron.setText(String.format("RON = %s", chfToAllCurrencies.get("ron")));
			chf.setText(String.format("CHF = %s", chfToAllCurrencies.get("chf")));
			gbp.setText(String.format("GBP = %s", chfToAllCurrencies.get("gbp")));
			break;
		case "GBP":
			HashMap<String, Double> gbpToAllCurrencies = (HashMap<String, Double>) Currency.GBP
					.convert(getInsertedVal());

			eur.setText(String.format("EUR = %s", gbpToAllCurrencies.get("eur")));
			usd.setText(String.format("USD = %s", gbpToAllCurrencies.get("usd")));
			ron.setText(String.format("RON = %s", gbpToAllCurrencies.get("ron")));
			chf.setText(String.format("CHF = %s", gbpToAllCurrencies.get("chf")));
			gbp.setText(String.format("GBP = %s", gbpToAllCurrencies.get("gbp")));
			break;
		default:
			// code block
		}
	}
}
